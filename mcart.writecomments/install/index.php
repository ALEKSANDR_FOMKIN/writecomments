<?php
global $DOCUMENT_ROOT, $MESS;

IncludeModuleLangFile( __FILE__);

Class mcart_writecomments extends CModule
{
	var $MODULE_ID = "mcart.writecomments";
	var $MODULE_VERSION;
	var $MODULE_VERSION_DATE;
	var $MODULE_NAME;
	var $MODULE_DESCRIPTION;
	var $MODULE_GROUP_RIGHTS = "Y";

	function __construct()
	{
		$arModuleVersion = array();

        $path = str_replace("\\", "/", __FILE__);
        $path = substr($path, 0, strlen($path) - strlen("/index.php"));
        include($path."/version.php");

        if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion)){
            $this->MODULE_VERSION = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        }

        $this->MODULE_NAME = GetMessage("mcart.writecomments_MODULE_NAME");
        $this->MODULE_DESCRIPTION = GetMessage("mcart.writecomments_MODULE_DESCRIPTION");

        $this->PARTNER_NAME = GetMessage("mcart.writecomments_PARTNER_NAME");
        $this->PARTNER_URI  = GetMessage("mcart.writecomments_PARTNER_URI");
	}
	

	function InstallDB() {
        $eventManager = \Bitrix\Main\EventManager::getInstance();
        $eventManager->registerEventHandler("blog", "OnCommentAdd",'mcart.writecommments', "Write", "OnAfterWriteMessageHandler");
		return true;

	}
	
    function UnInstallDB()
    {
        $eventManager = \Bitrix\Main\EventManager::getInstance();
        $eventManager->unRegisterEventHandler("blog", "OnCommentAdd", 'mcart.writecomments', "Write", "OnAfterWriteMessageHandler");
        return true;
    }

    function InstallEvents()
	{

		return true;
	}

	function UnInstallEvents()
	{

		return true;
	}

	function InstallFiles()
	{
	return true;
	}
	
	function UnInstallFiles()
	{	
		
		return true;
	}

    function DoInstall()
    {
        if (!IsModuleInstalled('mcart.writecomments'))
        {
            $this->InstallDB();
            $this->InstallEvents();
            $this->InstallFiles();
            RegisterModule('mcart.writecomments');
        }
    }

    function DoUninstall()
    {
        $this->UnInstallDB();
        $this->UnInstallEvents();
        $this->UnInstallFiles();
        UnRegisterModule('mcart.writecomments');
    }
}
?>